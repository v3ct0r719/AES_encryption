import random
from Crypto.Cipher import AES
import string
st=string.letters+string.digits
def key_or_iv_gen():
	s=""
	for i in range(16):
		s+=st[random.randint(0,(len(st)-1))]
	return s

key=key_or_iv_gen()
iv=key_or_iv_gen()

def encrypt(s):
	e=AES.new(key,AES.MODE_CBC,iv)
	return e.encrypt(s)

def decrypt(s):
	e=AES.new(key,AES.MODE_CBC,iv)
	return e.decrypt(s)

pt="Hell0 World !!!!"
ct=encrypt(pt)
at=ct+chr(0)*16+ct
x=decrypt(at)
print "The iv that we used in encryption",iv
print "The iv that we got is:","".join(chr(ord(a)^ord(b)) for a,b in zip(x[0:16],x[32:48]))



