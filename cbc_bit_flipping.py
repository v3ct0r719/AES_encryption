from Crypto.Cipher import AES
import os
key=os.urandom(16)
iv=os.urandom(16)
def pad(s,l):
	d=(l-len(s)%l)*chr(l-(len(s)%l))
	return s+d

def encrypt(j):
	e=AES.new(key,AES.MODE_CBC,iv)
	for i in j:
		if i==";" or i=="=" :
			j=j.replace(i,"*")
	return e.encrypt(pad("comment1=cooking%20MCs;userdata="+j+";comment2=%20like%20a%20pound%20of%20bacon",16))


def decrypt(v):
	e=AES.new(key,AES.MODE_CBC,iv)
	return e.decrypt(v
	)	

s=";admin=true;"
a=encrypt(s)

ciphertext=[]
for i in a:
	ciphertext.append(i)
ciphertext[16]=chr(ord(ciphertext[16])^ord('*')^ord(';'))			
ciphertext[22]=chr(ord(ciphertext[22])^ord('*')^ord('='))
ciphertext[27]=chr(ord(ciphertext[27])^ord('*')^ord(';'))

ct="".join(ciphertext)

result=decrypt(ct)
if 'admin=true' in result:
	print "login successful"
else:
	print "login failed"